json.extract! invoice, :id, :payment_meth, :total, :emission_date, :created_at, :updated_at
json.url invoice_url(invoice, format: :json)
