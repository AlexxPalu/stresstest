json.extract! order, :id, :cost, :quantity, :discount, :emission, :created_at, :updated_at
json.url order_url(order, format: :json)
