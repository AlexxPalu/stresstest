json.extract! article, :id, :name, :code, :weight, :created_at, :updated_at
json.url article_url(article, format: :json)
