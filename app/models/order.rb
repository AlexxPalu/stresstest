class Order
  include Mongoid::Document
  field :cost, type: Float
  field :quantity, type: Integer
  field :discount, type: Float
  field :emission, type: Time
  has_many :articles
  embeds_one :invoice
  embedded_in :user
end
