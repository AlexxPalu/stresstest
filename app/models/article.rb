class Article
  include Mongoid::Document
  field :name, type: String
  field :code, type: Integer
  field :weight, type: Float
  belongs_to :order
end
