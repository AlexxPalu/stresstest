class Invoice
  include Mongoid::Document
  field :payment_meth, type: String
  field :total, type: Float
  field :emission_date, type: Time
  embedded_in :order
end
