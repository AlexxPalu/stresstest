class User
  include Mongoid::Document
  include Mongoid::Attributes::Dynamic
  field :full_name, type: String
  field :email, type: String
  field :type, type: String
  # field :orders, type: Array
  embeds_many :orders
end

# user.orders = [ order ]
